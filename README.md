# README #

>Problem: Need Gravity forms to interface and share data with WP eMembers membership plugin

This solution was developed to allow data collection to take place in GravityForms while creating membership accounts in eMembers. There are several other tasks that have been created to solve integration issues between the two plugins.

## Custom behaviors added to Gravity Forms via this plugin ##

* Create new eMember affiliate record
* Create new mortgage sponsor record
* Create new realtor record
* Create new eMember member record
* Set new user's home directory in GF
* Set new affiliate's headshot upload directory in GF
* Create new eMember record after GF submission
* Auto-populate the following GF fields when form is first displayed via database query on a custom content type
    * email address
    * realtor license number
    * license state
    * license type
    * populate user's name
* Create composite username to allow advanced logic
* Set image upload path to custom directory  
* Rename uploaded headshot image name after save
* Auto login new user after eMember creation
* Display current user's headshot image
