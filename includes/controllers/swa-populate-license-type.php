<?php

/**
 * Load the user's license type (RE,MB)
 *
 * @param
 * @return    void string
 * @author B Burt
 * @copyright
 */

// * active * add_filter('gform_field_value_license_type', 'swa_populate_license_type');

// This is used so that field is read only
function swa_populate_license_type(){

    $current_user = wp_get_current_user();

    $current_user_id = $current_user->ID;

    $license_type = get_user_meta($current_user_id, $key = USER_META_LICENSE_TYPE, true);

    return $license_type;
}
