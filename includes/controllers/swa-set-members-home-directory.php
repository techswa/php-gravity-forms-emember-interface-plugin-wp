<?php


/**
 *
 * Create a new affilaite record for the user the gravity forms has just saved.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author B Burt
 * @copyright
 */

function swa_set_members_home_directory( $entry, $form ){

   // Get the user_name
   $user_name = rgar( $entry, '7' );

   // Get the newly created user
   $new_user = get_user_by( 'login', $user_name );

   //Get the user id of the newly created user
   $new_user_id = $new_user->ID;

   // Add the home directory to user meta
   add_user_meta($new_user_id, USER_META_HOME_DIRECTORY, $user_name);

}
