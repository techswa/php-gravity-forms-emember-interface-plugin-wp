<?php

/**
 * Automatically register new user after creation
 *
 * @param
 * @return    void
 * @author B Burt
 * @copyright
 */

// // Run for MB professional creation
//* active * add_action( 'gform_user_registered_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_login_new_user', 10, 3 );
// // Run for RE professional creation
//* active *  add_action( 'gform_user_registered_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_login_new_user', 10, 3 );


function swa_login_new_user( $user_id, $feed, $entry ) {

  $user_info = get_userdata($user_id);

  $user_login = $user_info->user_login;
  //Auto login the new user
  programmatic_login( $user_login );

}
