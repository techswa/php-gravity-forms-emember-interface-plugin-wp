<?php

/**
 * Load the user's email address
 *
 * @param
 * @return    string $email address
 * @author B Burt
 * @copyright
 */

// This is used so that field is read only
// * active *  add_filter('gform_field_value_email_address', 'swa_populate_email_address');

function swa_populate_email_address(){

  $current_user = wp_get_current_user();

  $email_address = $current_user->data->user_email;

  return $email_address;
}
