<?php


/**
 *
 * Create a new affilaite record for the user the gravity forms has just saved.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author B Burt
 * @copyright
 */

function swa_create_affiliate_record( $entry, $form ){
 global $wpdb;

 // $referrer = $_COOKIE['ap_id'];
 // If the referrer is empty than tie to house account_state
 if (isset($_COOKIE['ap_id'])){
   $referrer = $_COOKIE['ap_id'];
 } else {
   $referrer = HOUSE_ACCOUNT;
 };

 $signup_date = date("Y-m-d"); //yyyy-mm-dd - matches wp_affiliate
 $account_status = 'approved';
 $password_text = rgar( $entry, '10' );
 $password_hash = swa_wp_hash_password( $password_text );


 //Get the data that was used to create the user record
 //rgar() is a gravity forms function that parses the $entry(['key']) returning value

   $refid = rgar( $entry, '7' ); //user_name
   $password = $password_hash;
   $firstname = rgar( $entry, '3.3' );
   $lastname = rgar( $entry, '3.6' );
   $email = rgar( $entry, '8' );
   $date = $signup_date;
   $commission_level = '1';
   $account_status = $account_status;

   $affiliates_table_name = $wpdb->prefix . SWA_WP_AFF_AFFILIATES_TBL_NAME;

   // Create new affiliate record
   $sql = "INSERT INTO $affiliates_table_name ";
   $sql .= "(refid, pass, firstname, lastname, email, date, commissionlevel, referrer, account_status) ";
   $sql .= "VALUES ('$refid', '$password', '$firstname', '$lastname', '$email', '$date', '$commission_level', '$referrer', '$account_status')";

   //Insert new record
   $results = $wpdb->query($sql);

   if ( $results ) {
     return $results;
   } else {
     return false;
   }
}
