<?php

/**
 * Load the user's nmls number
 *
 * @param
 * @return    void
 * @author B Burt
 * @copyright
 */

// This is used so that field is read only
// * active * add_filter('gform_field_value_license_number', 'swa_populate_license_number');

function swa_populate_license_number(){

  $current_user = wp_get_current_user();

  $current_user_id = $current_user->ID;

  $license_number = get_user_meta($current_user_id, $key = USER_META_LICENSE_NUMBER, true);

  return $license_number;

}
