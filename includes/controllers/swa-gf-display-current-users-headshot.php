<?php

/**
 * Display the current users headshot inthe marketing form
 *
 * @param
 * @return    void
 * @author B Burt
 * @copyright
 */

// //Display the image when the form is shown
//* active * add_filter( 'gform_pre_render_' . GF_PROFESSIONAL_MARKETING_ID, 'swa_gf_display_current_users_headshot' );
//* active * add_filter( 'gform_admin_pre_render_' . GF_PROFESSIONAL_MARKETING_ID, 'swa_gf_display_current_users_headshot' );

 function swa_gf_display_current_users_headshot( $form ){

   // get the current user information
   $current_user = wp_get_current_user();

   //Get user id
   $user_id = $current_user->ID;

   // Get the users path & url for thier home directories
   $home_directory_info = swa_get_current_users_home_directory_info( $current_user );

   // Set vars for GF to save the file
   //$image_url = $home_directory_info['url'];

   // Get the user meta
   $nickname = get_user_meta($user_id, USER_META_NICKNAME, 'single');

   // Construct image path
   $image_path = $nickname . IMAGE_DIRECTORY;

   //Get the image file name
   $image_name = get_user_meta($user_id, USER_META_IMAGE_HEADSHOT, 'single');

  // WP upload Directory
  $upload_dir = wp_upload_dir();

  $upload_dir_url = $upload_dir['url']; //"http://localhost/wp-content/uploads"

  //Setup for local storage only - Doen't use user_home_directory
  $image_handle = $home_directory_info['url'] . IMAGE_DIRECTORY . '/' .  $image_name;

  // Set gf html div to display the image
  //get the field to display the image in
  $image_display_field = GFFormsModel::get_field( $form, 24 );

//get the html content
$image_display_field['content'] = "<div><img src='{$image_handle}'></div>";
  // Add gf hook for display

return $form;
 }
