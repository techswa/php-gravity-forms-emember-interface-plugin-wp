<?php
// This file contains constants that are used in SWA functions

//DATABASE
// Define 3rd Party tables
if ( !defined('SWA_WP_AFF_AFFILIATES_TBL_NAME') ) {
  define('SWA_WP_AFF_AFFILIATES_TBL_NAME', 'affiliates_tbl');
}
if ( !defined('SWA_WP_EMEMBER_MEMBERS_TABLE_NAME') ) {
  define("SWA_WP_EMEMBER_MEMBERS_TABLE_NAME", "wp_eMember_members_tbl");
}

//Define membership levels
define('MEMBERSHIP_LEVEL_REAL_ESTATE_PRO','7');
define('MEMBERSHIP_LEVEL_MORTGAGE_PRO','8');

//Gravity Form ID's
define("GF_CONSUMER_CREATE_ID", '1');
define("GF_MB_PROFESSIONAL_CREATE_ID", '3');
define("GF_RE_PROFESSIONAL_CREATE_ID", '24');

define("GF_PROFESSIONAL_SPONSOR_EDIT_ID", '28');

define("GF_PROFESSIONAL_CONTACTS_ID", '21');
define("GF_PROFESSIONAL_LICENSE_ID", '20');
define("GF_PROFESSIONAL_MARKETING_ID", '23');
define("GF_PROFESSIONAL_SOCIAL_MEDIA_ID", '23');
define("GF_PROFESSIONAL_USER_ID", '19');
