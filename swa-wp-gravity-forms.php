<?php

/*
Plugin Name: SWA EYCS Gravity Forms
Version: 1.0.0
Plugin URI:
Author:    B Burt
Author URI: http://www.smarterwebapps.com/
*/

// Exit if accessed directly.
if( !defined( 'ABSPATH' ) ) exit;

include_once('includes/constants.php');

include_once('includes/shortcodes-filters-actions.php');


// //Queries
include_once('includes/queries/swa-populate-states-and-abbreviations.php');

//Controllers
include_once('includes/controllers/swa-create-composite-user-name.php');
include_once('includes/controllers/swa-create-mortgage-pro-membership-records.php');
include_once('includes/controllers/swa-create-real-estate-pro-membership-records.php');

include_once('includes/controllers/swa-gf-user-login-after-register.php');
include_once('includes/controllers/swa-create-affiliate-record.php');
include_once('includes/controllers/swa-create-emember-record.php');
include_once('includes/controllers/swa-set-members-home-directory.php');
include_once('includes/controllers/swa-populate-email-address.php');
include_once('includes/controllers/swa-populate-license-number.php');
include_once('includes/controllers/swa-populate-user-name.php');
include_once('includes/controllers/swa-populate-license-type.php');
include_once('includes/controllers/swa-populate-license-state.php');
include_once('includes/controllers/swa-gf-set-image-headshot-upload-path.php');
include_once('includes/controllers/swa-gf-rename-image-headshot-file.php');
include_once('includes/controllers/swa-gf-display-current-users-headshot.php');
